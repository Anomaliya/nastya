#include <iostream>

int main() 
{
  using namespace std;
  const int feet_per_inch = 12;
  int tall;

  cout << "Enter your height in inches:___\b\b\b";
  cin >> tall;
  int feet = tall / feet_per_inch; // whole feet
  int inches = tall % feet_per_inch; // remainder in inches
  cout << tall << " inches is " << feet 
       << " feet, " << inches << " inches.\n"; 
  return(0);
}