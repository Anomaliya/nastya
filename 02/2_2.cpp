#include <iostream>
#include <cmath>
using namespace std;

int main() {
  cout.setf(ios_base::fixed, ios_base::floatfield);
  cout << "Enter your height in " << endl << "feet: ";
  float feet;
  cin >> feet;
  cout << "inches: ";
  float inch;
  cin >> inch;
  cout << "Enter your weight in pounds: " << endl;
  int pounds;
  cin >> pounds; 
  const int constInch = 12;
  const float constMeter = 0.0254;
  const float constkilo = 2.2;
  int height = ((feet * constInch) + inch) * constMeter;
  int weight = pounds / constkilo;
  int bmi = weight / pow (height, 2.0);
  cout << "bmi index: " << bmi << endl;
return(0);
}