#include <iostream>
using namespace std;

int main() {
  cout << "Enter a lattitude in degrees, minutes and seconds:" << endl;
  cout << "First, enter the degrees: ";
  float degrees;
  cin >> degrees;
  cout << "Next, enter the minutes of arc: ";
  float min;
  cin >> min;
  cout << "Finally, enter the seconds of arc: ";
  float sec;
  cin >> sec;
  const int count = 60;
  float lat = ((min + (sec / count)) / count) + degrees;
  cout << degrees << " degrees, " << min << " minutes, " << sec << " seconds = " << lat << " degrees" << endl;
  return(0);
  }