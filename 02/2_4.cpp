#include <iostream>

int main() 
{
  using namespace std;

  const int sec_per_day = 86400;
  const int sec_per_hour = 3600;
  const int sec_per_min = 60;

  cout << "Enter the number of seconds: ";
  long long input_seconds;
  cin >> input_seconds;
  
  int days = input_seconds / sec_per_day; // whole days
  int rem_days = input_seconds % sec_per_day; // remainder in seconds per day
  
  int hours = rem_days / sec_per_hour; // whole hours
  int rem_hours = rem_days % sec_per_hour; // remainder in seconds per hour
  
  int minuts = rem_hours / sec_per_min; // whole minuts
  int rem_min = rem_hours % sec_per_min; // remainder in seconds per minuts
  
  cout << input_seconds << " seconds = " 
       << days << " days, " << hours << " hours, "
       << minuts << " minuts, " 
       << rem_min << " seconds.\n";
  return(0);
}