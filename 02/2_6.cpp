#include <iostream>

int main() 
{
  using namespace std;
  float dist_miles;
  cout << "Enter the distance in miles ";
  cin >> dist_miles;
  float num_gal;
  cout << "Enter the fuel in gallons ";
  cin >> num_gal;
  float miles_per_gal = dist_miles / num_gal;
  cout << miles_per_gal << " miles per gallon " << endl;
  return(0);
}