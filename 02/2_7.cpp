#include <iostream>

int main() 
{
  using namespace std;
  const double mile = 62.14; // 100 km in miles
  double gallon = 3.875f; // 1 gallon in liters
  float consumption;
  cout << "Enter the consumption of gasoline in liters per 100 km ";
  cin >> consumption;
  double litr_per_gal = consumption / gallon;
  int miles_per_gal = mile / litr_per_gal;
  cout << consumption << " liter per km is about " 
       << miles_per_gal << " miles per gallon.\n";
  return(0);
}